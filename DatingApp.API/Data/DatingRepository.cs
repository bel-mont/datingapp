using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data
{
  public class DatingRepository : IDatingRepository
  {
    private readonly DataContext _context;
    public DatingRepository(DataContext context)
    {
      _context = context;

    }
    public void Add<T>(T entity) where T : class
    {
      _context.Add(entity);
    }

    public void Delete<T>(T entity) where T : class
    {
      _context.Remove(entity);
    }

    public async Task<User> GetUser(int id)
    {
      var user = await _context.Users.Include(x => x.Photos).FirstOrDefaultAsync(x => x.Id == id);
      return user;
    }

    public async Task<PagedList<User>> GetUsers(UserParams userParams)
    {
      var users = _context.Users.Include(x => x.Photos).OrderByDescending(x => x.LastActive).AsQueryable();
      users = users.Where(x => x.Id != userParams.UserId && x.Gender == userParams.Gender);
      if (userParams.MinAge != 18 || userParams.MaxAge != 99)
      {
        var minDateOfBirth = DateTime.Today.AddYears(-userParams.MaxAge - 1);
        var maxDateOfBirth = DateTime.Today.AddYears(-userParams.MinAge);
        users = users.Where(x => x.DateOfBirth >= minDateOfBirth && x.DateOfBirth <= maxDateOfBirth);
      }

      if (!string.IsNullOrEmpty(userParams.OrderBy))
      {
        users = userParams.OrderBy switch
        {
          "created" => users.OrderByDescending(x => x.Created),
          _ => users.OrderByDescending(x => x.LastActive)
        };
      }
      return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
    }

    public async Task<bool> SaveAll()
    {
      return await _context.SaveChangesAsync() > 0;
    }

    public async Task<Photo> GetPhoto(int id)
    {
      var photo = await _context.Photos.FirstOrDefaultAsync(x => x.Id == id);
      return photo;
    }

    public async Task<Photo> GetMainPhotoForUser(int userId)
    {
      var photo = await _context.Photos.FirstOrDefaultAsync(x => x.UserId == userId && x.IsMain);
      return photo;
    }
  }
}